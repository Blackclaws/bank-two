using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accounts.Infrastructure;
using Accounts.Messages;
using Accounts.Model;
using Accounts.Service.Api.Base.Controllers;
using Accounts.Service.Api.Base.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using PoliceService.ApiClient.Api;

namespace Accounts.Controllers {
    [ApiController]
    public class AccountsController : AccountsApiController
    {

        AccountsRepository _repo;
        private AccountsControllerConfig _config;
        private PoliceApi _policeApi;
        private MessageBusBroker _broker;

        public AccountsController(AccountsRepository repo, IOptions<AccountsControllerConfig> config, PoliceApi policeApi, MessageBusBroker broker)
        {
            _repo = repo;
            _config = config.Value;
            _policeApi = policeApi;
            _broker = broker;
        }

        public override async Task<IActionResult> AccountsChangeBalance(int id, ChangeAmountRequest changeAmountRequest)
        {
            var accounts = await _repo.GetByIdAsync(id);
            accounts.Balance += changeAmountRequest.Amount;
            // Notify anyone of a change
            _broker.Send(new AccountMoneyChangedEvent(id, changeAmountRequest.Amount));

            await _repo.UpdateAsync(accounts);
            return Ok();
        }

        public override async Task<IActionResult> AccountsGet()
        {
            var accounts = await _repo.ListAsync();
            var dtos = accounts.Select(x => new AccountDTO()
            {
                Id = x.Id,
                Balance = x.Balance * _config.Scaling,
                Owner = x.Owner
            }).ToList();
            await Task.Delay(_config.Delay);
            return new ObjectResult(dtos);
        }

        public override async Task<IActionResult> AccountsGetBalanceById(int id)
        {
            var accounts = await _repo.GetByIdAsync(id);
            var response = new AccountGetBalanceByIdResponse()
            {
                Balance = accounts.Balance
            };
            return new ObjectResult(response);
        }
    }
}