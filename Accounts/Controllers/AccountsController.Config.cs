﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounts.Controllers
{
    public class AccountsControllerConfig
    {
        public double Scaling { get; set; }
        public int Delay { get; set; }
    }
}
