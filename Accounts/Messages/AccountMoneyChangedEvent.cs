﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounts.Messages
{
    public class AccountMoneyChangedEvent
    {
        public AccountMoneyChangedEvent(int accountId, double amount)
        {
            AccountId = accountId;
            Amount = amount;
        }
        public int AccountId { get; set; }
        public double Amount { get; set; }
    }
}
