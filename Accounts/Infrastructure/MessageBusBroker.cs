﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Accounts.Messages;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Accounts.Infrastructure
{
    public class MessageBusBroker
    {
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channel;

        public MessageBusBroker()
        {
            _factory = new ConnectionFactory() {HostName = "rabbitmq"};
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: "MoneyQueue", durable: false, exclusive: false, autoDelete: false);
        }

        public void Send(AccountMoneyChangedEvent eventToSend)
        {
                var body =JsonSerializer.SerializeToUtf8Bytes(eventToSend, eventToSend.GetType(), new JsonSerializerOptions()
                {
                    WriteIndented = true
                });
                var properties = _channel.CreateBasicProperties();
                properties.DeliveryMode = 2;
                _channel.BasicPublish("", routingKey: "MoneyQueue", basicProperties: properties, body: body);
        }
    }
}
