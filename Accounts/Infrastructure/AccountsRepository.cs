using Accounts.Model;
using Microsoft.EntityFrameworkCore;

namespace Accounts.Infrastructure {

    public class AccountsRepository : Ardalis.Specification.EntityFrameworkCore.RepositoryBase<BankAccount>
    {
        public AccountsRepository(AccountsDBContext dbContext) : base(dbContext)
        {
        }
    }
}