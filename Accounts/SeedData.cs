﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accounts.Infrastructure;

namespace Accounts
{
    public class SeedData
    {
        public static void SeedDb(AccountsDBContext context)
        {
            if (!context.BankAccounts.Any())
            {
                context.BankAccounts.Add(new Model.BankAccount()
                {
                    Owner = "Dagobert",
                    Balance = 1e10
                });

                context.BankAccounts.Add(new Model.BankAccount()
                {
                    Owner = "Donald",
                    Balance = 120
                });

                context.BankAccounts.Add(new Model.BankAccount()
                {
                    Owner = "Panzerknacker",
                    Balance = -1500
                });

                context.SaveChanges();
            }
        }
    }
}
