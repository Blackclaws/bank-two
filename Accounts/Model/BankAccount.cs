﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounts.Model
{
    public class BankAccount
    {
        public int Id { get; set; }
        public double Balance { get; set; }
        public string Owner { get; set; }
    }
}
