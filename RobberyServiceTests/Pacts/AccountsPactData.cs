﻿using System;
using PactNet;
using PactNet.Mocks.MockHttpService;
using PactNet.Models;

namespace FrontendTests.Pacts
{
    public class AccountsPactData : IDisposable
    {
        public IPactBuilder Builder { get; init; }
        public IMockProviderService MockProviderService { get; }

        public int MockServerPort => 9222;
        public string MockProviderServiceBaseUri => $"http://localhost:{MockServerPort}";

        public AccountsPactData()
        {
            Builder = new PactBuilder(new PactConfig()
            {
                PactDir = $"../../../../pacts"
            }).ServiceConsumer("RobberyService").HasPactWith("Accounts");
            MockProviderService = Builder.MockService(MockServerPort, false, IPAddress.Any);
        }

        public void Dispose()
        {
            Builder.Build();
        }
    }
}
