﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Accounts.ApiClient.Api;
using Accounts.ApiClient.Client;
using PactNet.Matchers;
using PactNet.Mocks.MockHttpService;
using PactNet.Mocks.MockHttpService.Models;
using RobberyService;
using Xunit;

namespace FrontendTests.Pacts
{
    public class AccountsConsumerTests : IClassFixture<AccountsPactData>
    {
        public IMockProviderService _mockProvider;
        public string _mockProviderServiceBase;

        public AccountsConsumerTests(AccountsPactData data)
        {
            _mockProvider = data.MockProviderService;
            _mockProviderServiceBase = data.MockProviderServiceBaseUri;
            _mockProvider.ClearInteractions();
        }

        class AccountDTO
        {
            public int Id { get; set; }
            public string Owner { get; set; }
            public double Balance { get; set; }
        }

        [Fact]
        public async Task GetAccountInfoForDagobert()
        {
            _mockProvider.Given("There exists an account with id 1 and a balance of 10000")
                .UponReceiving("a request to retrieve accounts")
                .With(new ProviderServiceRequest()
                {
                    Method = HttpVerb.Get,
                    Path = "/accounts/1",
                    Headers = new Dictionary<string, object>
                    {
                        {"Accept", "application/json"},
                    }
                })
                .WillRespondWith(new ProviderServiceResponse()
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        {"Content-Type", "application/json; charset=utf-8"}
                    },
                    Body =
                        new
                        {
                            balance = 10000
                        }
                });

            var service = new AccountsService(new AccountsApi(new Configuration()
            {
                BasePath = _mockProviderServiceBase
            }));
            var res = await service.GetAccountInfoAsync(1);

            _mockProvider.VerifyInteractions();

            Assert.True(res.amountOfMoney == 10000);
        }
        
        [Fact]
        public async Task CanDeductMoneyFromAccount() {
            _mockProvider.Given("There exists an account with id 1 and a balance of 10000")
                .UponReceiving("a request to retrieve accounts")
                .With(new ProviderServiceRequest()
                {
                    Method = HttpVerb.Post,
                    Path = "/accounts/1/changeBalance",
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8"},
                    },
                    Body = new
                    {
                        Amount = -10000
                    }
                    
                })
                .WillRespondWith(new ProviderServiceResponse()
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                    },
                });

            var service = new AccountsService(new AccountsApi(new Configuration()
            {
                BasePath = _mockProviderServiceBase
            }));
            await service.DeductMoneyFromAccount(1, 10000);

            _mockProvider.VerifyInteractions();

        }
    }
}
