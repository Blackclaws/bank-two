rm -r generated/
docker run -it --rm -v "${PWD}:/openapi" `
       openapitools/openapi-generator-cli `
       generate `
       -i /openapi/Accounts/ApiSpec/Accounts.ApiSpec.yml `
       -g csharp-netcore `
       -o /openapi/generated `
       -c /openapi/AccountsApiSpecConfig.yml

docker run -it --rm  -v "${PWD}:/openapi" `
       openapitools/openapi-generator-cli `
       generate `
       -i /openapi/specs/Robbery.ApiSpec.yml `
       -g csharp-netcore `
       -o /openapi/generated `
       -c /openapi/RobberyApiSpecConfig.yml
	   
docker run -it --rm  -v "${PWD}:/openapi" `
       openapitools/openapi-generator-cli `
       generate `
       -i /openapi/specs/Police.ApiSpec.yml `
       -g csharp-netcore `
       -o /openapi/generated `
       -c /openapi/PoliceApiSpecConfig.yml