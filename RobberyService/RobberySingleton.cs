﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace RobberyService
{
    public class RobberySingleton
    {
        public List<Robbery> RunningRobberies = new List<Robbery>();
        private AccountsService _accountsService;
        private Random random;

        public RobberySingleton(AccountsService accountsService)
        {
            _accountsService = accountsService;
            random = new Random();
        }

        public Robbery StartNewRobbery()
        {
            var robbery = new Robbery();
            robbery.Id = random.Next(0, 10000);
            robbery.AmountCollected = 0;
            RunningRobberies.Add(robbery);
            return robbery;
        }

        public async Task<double> RobAccount(int id, int? accountId)
        {
            var robbery = RunningRobberies.FirstOrDefault(x => x.Id == id);
            if (robbery == null)
            {
                throw new ArgumentException("Robbery not found");
            }

            var accountInfo = await _accountsService.GetAccountInfoAsync(accountId);
            var robbedMoney = random.NextDouble() * accountInfo.amountOfMoney;
            await _accountsService.DeductMoneyFromAccount(accountId, robbedMoney);
            robbery.AmountCollected += robbedMoney;
            return robbedMoney;
        }

        public async Task Stash(int id, int? accountId)
        {
            var robbery = RunningRobberies.FirstOrDefault(x => x.Id == id);
            if (robbery == null)
            {
                throw new ArgumentException("Robbery not found");
            }

            await _accountsService.PutMoneyIntoAccount(accountId, robbery.AmountCollected);
            RunningRobberies.Remove(robbery);
        }
    }
}
