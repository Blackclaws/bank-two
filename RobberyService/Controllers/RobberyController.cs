﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Robbery.Service.Api.Base.Controllers;
using Robbery.Service.Api.Base.Models;

namespace RobberyService.Controllers
{
    public class RobberyController : RobberyApiController
    {
        private RobberySingleton robberCentral;
        private ActivitySource aSource = new ActivitySource(nameof(RobberyController));

        public RobberyController(RobberySingleton robber)
        {
            robberCentral = robber;
        }

        public override async Task<IActionResult> Rob(int id, int? accountId)
        {
            using (Activity? robActivity = aSource.StartActivity("Robbing"))
            {
                try
                {
                    var directRobResult = await robberCentral.RobAccount(id, accountId);
                    var result = new RobberyRobResult()
                    {
                        RobbedAmount = directRobResult
                    };
                    return new ObjectResult(result);
                }
                catch (Exception e)
                {
                    return BadRequest("Something went wrong");
                }

                robActivity?.Stop();
            }
        }

        public override async Task<IActionResult> StartRobbery()
        {
            Robbery newRobbery = robberCentral.StartNewRobbery();
            return new ObjectResult( new RobberyStartedDTO()
            {
                Id = newRobbery.Id
            });
        }

        public override async Task<IActionResult> Stash(int id, int? accountId)
        {
            try
            {
                 await robberCentral.Stash(id, accountId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest("Something went wrong");
            }

        }
    }
}
