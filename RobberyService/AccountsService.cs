﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accounts.ApiClient.Api;
using Accounts.ApiClient.Model;

namespace RobberyService
{
    public class AccountsService
    {
        private AccountsApi _apiClient;
        public AccountsService(AccountsApi apiClient)
        {
            _apiClient = apiClient;
        }

        public async Task<AccountInfo> GetAccountInfoAsync(int? accountId)
        {
            var response = await _apiClient.AccountsGetBalanceByIdAsync(accountId.Value);
            return new AccountInfo()
            {
                amountOfMoney = response.Balance
            };
        }

        public async Task DeductMoneyFromAccount(int? accountId, double robbedMoney)
        {
            await _apiClient.AccountsChangeBalanceAsync(accountId.Value, new ChangeAmountRequest(-1*robbedMoney));
            return;
        }

        public async Task PutMoneyIntoAccount(int? accountId, double robberyAmountCollected)
        {
            await _apiClient.AccountsChangeBalanceAsync(accountId.Value, new ChangeAmountRequest(robberyAmountCollected));
        }
    }

    public class AccountInfo
    {
        public double amountOfMoney;
    }
}
