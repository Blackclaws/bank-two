using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Accounts.ApiClient.Api;
using Accounts.ApiClient.Client;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using RobberyService.Controllers;

namespace RobberyService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSingleton<RobberySingleton>();
            services.AddSingleton<AccountsService>();
            services.AddHttpClient();
            services.AddSingleton<AccountsApi>(x =>
            {
                return new AccountsApi(x.GetService<HttpClient>(), new Configuration()
                {
                    BasePath = "http://accounts"
                });
            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "RobberyService", Version = "v1" });
            });
            services.AddOpenTelemetryTracing( builder =>
            {
                builder.SetSampler(new AlwaysOnSampler());
                builder.AddHttpClientInstrumentation(x =>
                {
                });
                builder.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("RobberyService"));
                builder.AddSource(nameof(RobberyController));
                builder.AddAspNetCoreInstrumentation();
                builder.AddJaegerExporter(x =>
                {
                    x.AgentHost = (Configuration.GetSection("Tracing").GetValue<string>("JaegerHost"));
                    x.AgentPort = (Configuration.GetSection("Tracing").GetValue<int>("JaegerPort"));
                });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "RobberyService v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
