using Frontend.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Frontend.ApiAccessors;
using Frontend.Pages;
using OpenTelemetry.Resources;
using OpenTelemetry.Trace;
using Robbery.ApiClient.Api;
using Robbery.ApiClient.Client;

namespace Frontend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();
            services.AddScoped<AccountsApiAccessor>(x =>
            {
                var accessor = new AccountsApiAccessor(x.GetRequiredService<HttpClient>())
                {
                    BaseUrl = "http://accounts:80"
                };
                return accessor;
            });
            services.AddScoped<RobberyApiAccessor>(x =>
            {
                var accessor = new RobberyApiAccessor(x.GetRequiredService<HttpClient>());
                return accessor;
            });
            services.AddScoped<RobberyApi>(x =>
            {
                var api = new RobberyApi(x.GetRequiredService<HttpClient>(), new Configuration()
                {
                    BasePath = "http://robbery"
                });
                return api;
            });
            services.AddScoped<AccountsService>();
            services.AddScoped<RobberyService>();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddOpenTelemetryTracing( builder =>
            {
                builder.SetSampler(new AlwaysOnSampler());
                builder.AddHttpClientInstrumentation(x =>
                {
                });
                builder.SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("Frontend"));
                builder.AddSource(nameof(Accounts));
                builder.AddAspNetCoreInstrumentation();
                builder.AddJaegerExporter(x =>
                {
                    x.AgentHost = (Configuration.GetSection("Tracing").GetValue<string>("JaegerHost"));
                    x.AgentPort = (Configuration.GetSection("Tracing").GetValue<int>("JaegerPort"));
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}
