﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Frontend.ApiAccessors;
using Robbery.ApiClient.Api;

namespace Frontend.Data
{
    public class RobberyService
    {
        public bool RobberyInProgress { get; set; } = false;
        public int RobberyId { get; set; } = -1;

        private RobberyApiAccessor _api;
        private RobberyApi _otherApi;

        public RobberyService(RobberyApiAccessor api, RobberyApi apitools_accessor)
        {
            _api = api;
            _otherApi = apitools_accessor;
        }

        public async Task StartRobbery()
        {
            RobberyInProgress = true;
            var robberyResult = await _otherApi.StartRobberyAsync();
            RobberyId = robberyResult.Id;
        }

        public async Task<double> RobAccount(int accountId)
        {
            // Does not work with NSwag https://github.com/RicoSuter/NSwag/issues/2419
            // var result = await _api.RobAsync(accountId, JsonSer);
            var result = await _otherApi.RobAsync(RobberyId, accountId);
            return result.RobbedAmount;
        }

        public async Task StashLootAndEndRobbery(int accountId)
        {
            await _otherApi.StashAsync(RobberyId, accountId);
            RobberyInProgress = false;
        }
    }
}
