using System;
using System.Linq;
using System.Threading.Tasks;
using Frontend.ApiAccessors;

namespace Frontend.Data
{
    public class AccountsService
    {
        private AccountsApiAccessor _accessor;

        public AccountsService(AccountsApiAccessor accessor)
        {
            _accessor = accessor;
        }

        public async Task<AccountDTO[]> GetAccountsAsync()
        {
            var accounts = await _accessor.AccountsAsync();
            return accounts.ToArray();
        }
    }
}
