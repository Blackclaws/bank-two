﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Accounts.Messages;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Accounts.Infrastructure
{
    public class MessageBusBroker : BackgroundService
    {
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channel;
        private ILogger<MessageBusBroker> _logger;

        public MessageBusBroker(ILogger<MessageBusBroker> logger)
        {
            _factory = new ConnectionFactory() {HostName = "rabbitmq"};
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: "MoneyQueue", durable: false, exclusive: false, autoDelete: false);
            _logger = logger;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {

            stoppingToken.ThrowIfCancellationRequested();
            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += async (ch, ea) =>
            {
                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                var moneyChanged = JsonSerializer.Deserialize<AccountMoneyChangedEvent>(content, new JsonSerializerOptions()
                {
                    PropertyNameCaseInsensitive = true
                });

                _logger.LogInformation("Received Money changed for account {accountId} amount {@amount}", moneyChanged.accountId, moneyChanged.amount);
                await Task.Delay(2000);

                _channel.BasicAck(ea.DeliveryTag, false);
            };

            _channel.BasicConsume("MoneyQueue", false, consumer);

            return Task.CompletedTask;
        }
    }
}
