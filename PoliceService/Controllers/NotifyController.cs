﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Police.Service.Api.Base.Controllers;
using Swashbuckle.AspNetCore.Annotations;

namespace PoliceService.Controllers
{
    public class NotifyController : PoliceApiController
    {
        private ILogger<NotifyController> _logger;
        public NotifyController(ILogger<NotifyController> logger)
        {
            _logger = logger;
        }

        public override async Task<IActionResult> Notify(int accountId, double amount)
        {
            // Police takes it slow
            await Task.Delay(2000);

            _logger.LogInformation($"{accountId} just had their money changed by {amount}");

            // But always signs off
            return Ok();
        }
    }
}
