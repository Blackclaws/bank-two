﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accounts.Messages
{
    public class AccountMoneyChangedEvent
    {
        public int accountId { get; set; }
        public double amount { get; set; }
    }
}
