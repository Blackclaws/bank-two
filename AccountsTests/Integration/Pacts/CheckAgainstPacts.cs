﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accounts;
using Accounts.Infrastructure;
using Accounts.Model;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PactNet;
using PactNet.Infrastructure.Outputters;
using Xunit;
using Xunit.Abstractions;
using IOutput = PactNet.Infrastructure.Outputters.IOutput;

namespace AccountsTests.Integration.Pacts
{
    public class CheckAgainstPactTests : 
 IDisposable
    {
        private readonly ITestOutputHelper _output;

        public CheckAgainstPactTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public async Task EnsureAccountsApiHonoursPactWithConsumer()
        {
            //Arrange
            const string serviceUri = "http://localhost:9222";
            var config = new PactVerifierConfig
            {
                Outputters = new List<IOutput>
                {
                    new XUnitOutput(_output)
                },
            };

            var hostBuilder = new HostBuilder().UseServiceProviderFactory(new AutofacServiceProviderFactory())
                .ConfigureWebHostDefaults(webHost =>
                {
                    webHost.UseUrls(serviceUri);
                    webHost.UseStartup<Startup>();
                    webHost.UseSolutionRelativeContentRoot("Accounts/")
                        .ConfigureServices(services =>
                        {
                            // Remove the app's ApplicationDbContext registration.
                            var descriptor = services.SingleOrDefault(
                                d => d.ServiceType ==
                                     typeof(DbContextOptions<AccountsDBContext>));

                            if (descriptor != null)
                            {
                                services.Remove(descriptor);
                            }

                            // This should be set for each individual test run
                            string inMemoryCollectionName = Guid.NewGuid().ToString();

                            // Add ApplicationDbContext using an in-memory database for testing.
                            services.AddDbContext<AccountsDBContext>(options =>
                            {
                                options.UseInMemoryDatabase(inMemoryCollectionName);
                            });

                        });
                });
            var host = hostBuilder.Build();
            using (var scope = host.Services.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<AccountsDBContext>();

                // Ensure the database is created.
                db.Database.EnsureCreated();

                try
                {
                    TestData.SeedDb(db);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed seeding db");
                }
            }


            await host.StartAsync();

            //Act / Assert
            IPactVerifier pactVerifier = new PactVerifier(config);
            pactVerifier
                .ServiceProvider("Accounts Service", serviceUri)
                .HonoursPactWith("Frontend")
                .PactUri("../../../../pacts/frontend-accounts.json")
                .Verify();

            pactVerifier = new PactVerifier(config);
            
            pactVerifier
                .ServiceProvider("Accounts Service", serviceUri)
                .HonoursPactWith("RobberyService")
                .PactUri("../../../../pacts/robberyservice-accounts.json")
                .Verify();

            await host.StopAsync();
        }

        public virtual void Dispose()
        {
        }
    }
}
