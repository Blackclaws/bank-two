﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Accounts;
using Accounts.Controllers;
using Accounts.Service.Api.Base.Models;
using Xunit;

namespace AccountsTests.Integration.Tests
{
    public class AccountsList : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;

        public AccountsList(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task ReturnsAccounts()
        {
            var result = await _client.GetAsync("/accounts");

            var content = await result.Content.ReadAsStringAsync();

            var accounts = JsonSerializer.Deserialize<List<AccountDTO>>(content, new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            });

            Assert.Contains(accounts, i => i.Owner == "Dagobart" && i.Balance == 10000);
        }
    }

}
