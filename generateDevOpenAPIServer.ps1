rm -r serverStubs/
docker run --rm -v "${PWD}:/openapi" `
       openapitools/openapi-generator-cli `
       generate `
       -i /openapi/specs/Robbery.ApiSpec.yml `
       -g aspnetcore `
       -o /openapi/serverStubs `
       -c /openapi/RobberyApiSpecServerConfig.yml

docker run --rm -v "${PWD}:/openapi" `
       openapitools/openapi-generator-cli `
       generate `
       -i /openapi/Accounts/ApiSpec/Accounts.ApiSpec.yml `
       -g aspnetcore `
       -o /openapi/serverStubs `
       -c /openapi/AccountsApiSpecServerConfig.yml

docker run --rm -v "${PWD}:/openapi" `
       openapitools/openapi-generator-cli `
       generate `
       -i /openapi/specs/Police.ApiSpec.yml `
       -g aspnetcore `
       -o /openapi/serverStubs `
       -c /openapi/PoliceApiSpecServerConfig.yml