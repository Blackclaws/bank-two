﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Frontend.ApiAccessors;
using Frontend.Data;
using PactNet.Matchers;
using PactNet.Mocks.MockHttpService;
using PactNet.Mocks.MockHttpService.Models;
using Xunit;

namespace FrontendTests.Pacts
{
    public class AccountsConsumerTests : IClassFixture<AccountsPactData>
    {
        public IMockProviderService _mockProvider;
        public string _mockProviderServiceBase;

        public AccountsConsumerTests(AccountsPactData data)
        {
            _mockProvider = data.MockProviderService;
            _mockProviderServiceBase = data.MockProviderServiceBaseUri;
            _mockProvider.ClearInteractions();
        }

        class AccountDTO
        {
            public int Id { get; set; }
            public string Owner { get; set; }
            public double Balance { get; set; }
        }

        [Fact]
        public async Task GetAllAcountsShouldReturnDagobert()
        {
            _mockProvider.Given("There exists an account with id 1 called dagobert and a balance of 10000")
                .UponReceiving("a request to retrieve accounts")
                .With(new ProviderServiceRequest()
                {
                    Method = HttpVerb.Get,
                    Path = "/accounts",
                    Headers = new Dictionary<string, object>
                    {
                        { "Accept", "application/json" },
                    }

                })
                .WillRespondWith(new ProviderServiceResponse()
                {
                    Status = 200,
                    Headers = new Dictionary<string, object>
                    {
                        { "Content-Type", "application/json; charset=utf-8" }
                    },
                    Body = 
                        Match.MinType(
                        new
                        {
                            id = 1,
                            owner = "Dagobert",
                            balance = 10000
                        }, 1)
                });

            var service = new AccountsService(new AccountsApiAccessor(new HttpClient())
            {
                BaseUrl = _mockProviderServiceBase
            });
            var res = await service.GetAccountsAsync();

            _mockProvider.VerifyInteractions();

            Assert.NotEmpty(res);
        }
    }
}
